import sanitize from "sanitize-filename";
import shortid from "shortid";

const { Storage } = require("@google-cloud/storage");
const { Datastore } = require("@google-cloud/datastore");

// Instantiate a storage client
const storage = new Storage();
const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET);

// Your Google Cloud Platform project ID
const projectId = process.env.GCLOUD_PROJECT_ID;

// Creates a client
const datastore = new Datastore({
  projectId,
});

interface IAbstractBlob {
  name: string;
  description?: string;
  thumbnailUrl: string;
  dataUrl: string;
  uploadedAt: number;
  previousVersion: string | undefined;
}

async function uploadFile(
  name: string,
  data,
  contentType: string,
  gzip = false
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    // Create a new blob in the bucket and upload the file data.
    const blob = bucket.file(name);
    const blobStream = blob.createWriteStream({
      resumable: false,
      contentType,
      gzip,
    });

    blobStream.on("error", (err) => {
      reject(err);
    });

    blobStream.on("finish", () => {
      resolve(`https://storage.googleapis.com/${bucket.name}/${blob.name}`);
    });

    blobStream.end(data);
  });
}

export async function createAbstractBlob(
  dataBaseName: string,
  data: string,
  imageData: string,
  name: string,
  description: string,
  previousVersion: string | undefined,
  filetype: "rrcs" | "json"
): Promise<string> {
  const id = sanitize(shortid.generate());

  let dataUrl;
  if (filetype === "rrcs") {
    dataUrl = await uploadFile(
      `circuit-${id}.rrcs`,
      Buffer.from(data, "base64"),
      "application/octet-stream"
    );
  } else {
    dataUrl = await uploadFile(
      `circuit-v2-${id}.json`,
      data,
      "application/json",
      true
    );
  }

  const thumbnailUrl = await uploadFile(
    `thumbnail-${id}.jpg`,
    Buffer.from(imageData, "base64"),
    "image/jpeg"
  );

  // The Cloud Datastore key for the new entity
  const taskKey = datastore.key([dataBaseName, id]);

  // Prepares the new entity
  const task = {
    key: taskKey,
    data: {
      description,
      name,
      dataUrl,
      thumbnailUrl,
      uploadedAt: Date.now(),
      previousVersion,
    },
  };

  // Saves the entity
  await datastore.save(task);
  console.log(`Saved ${task.key.name}: ${task.data.description}`);

  return id;
}

export async function getAbstractBlobData(
  dataBaseName: string,
  id
): Promise<IAbstractBlob> {
  const data = await datastore.get(datastore.key([dataBaseName, id]));

  return data[0];
}
