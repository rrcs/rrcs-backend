import { createAbstractBlob, getAbstractBlobData } from "./abstract-blob";

export function createCircuitV2Blob(
  data,
  imageData,
  name,
  description,
  previousVersion
) {
  return createAbstractBlob(
    process.env.CIRCUITS_V2_ENTITY_NAME,
    data,
    imageData,
    name,
    description,
    previousVersion,
    "json"
  );
}

export function getCircuitV2BlobData(id) {
  return getAbstractBlobData(process.env.CIRCUITS_V2_ENTITY_NAME, id);
}
