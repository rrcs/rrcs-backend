import { createAbstractBlob, getAbstractBlobData } from "./abstract-blob";

export function createCircuitBlob(data, imageData, name, description) {
  return createAbstractBlob(
    process.env.CIRCUIT_ENTITY_NAME,
    data,
    imageData,
    name,
    description,
    undefined,
    "rrcs"
  );
}

export function getCircuitBlobData(id) {
  return getAbstractBlobData(process.env.CIRCUIT_ENTITY_NAME, id);
}
