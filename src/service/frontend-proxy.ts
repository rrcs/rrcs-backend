import proxy from "express-http-proxy";
import { URL } from "url";

export function frontendProxy(path: string) {
  const url = new URL(path);

  return proxy(url.host, {
    https: true,
    proxyReqPathResolver: (req) => {
      console.log("Proxy to: ", url.pathname, req.url);
      return url.pathname + req.url;
    },
  });
}
