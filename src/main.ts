import "idempotent-babel-polyfill";
import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import http from "http";
import cors from "cors";
import favicon from "serve-favicon";
import { rootRouter } from "./routes/routes";

const app = express();
const server = http.createServer(app);

app.use(cors());
app.use(cookieParser());
app.use(
  bodyParser.json({
    limit: "1mb",
  })
);
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "1mb",
  })
);

app.set("views", __dirname + "/views");
app.set("view engine", "pug");

// appengine server check
app.get("/_ah/*", (req, res) => {
  res.send("ok");
});

app.use(favicon("./assets/favicon.ico"));
app.use("/", rootRouter);

app.use("*", (req, res) => {
  console.error("HTTP-404: ", req.url);
  res.status(404).send("not found");
});

function main() {
  // Start Server
  const port = process.env.PORT || 80;
  server.listen(port, () => {
    console.log("Server listening on PORT: " + port);
  });
}

main();
