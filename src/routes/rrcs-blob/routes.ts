import {
  createCircuitBlob,
  getCircuitBlobData,
} from "../../service/database/rrcs-blob";
import { Router } from "express";

export const rrcsBlobRouter = Router();

rrcsBlobRouter.get("/:blobId", async (req, res) => {
  try {
    const blob = await getCircuitBlobData(req.params.blobId);
    if (blob) {
      res.send(blob);
    } else {
      res.status(404).send(`circuit "${req.params.blobId}" not found`);
    }
  } catch (err) {
    console.error(err);
    res.status(404).send(`circuit "${req.params.blobId}" not found`);
  }
});

rrcsBlobRouter.post("/", async (req, res) => {
  try {
    if (!req.body.data || !req.body.data.length) {
      res.status(400).send("missing data");
    } else if (!req.body.thumbnail || !req.body.thumbnail.length) {
      res.status(400).send("missing thumbnail");
    } else {
      const id = await createCircuitBlob(
        req.body.data,
        req.body.thumbnail,
        req.body.name,
        req.body.description
      );

      res.send(id);
    }
  } catch (err) {
    console.error(err);
    res.status(500).send("error");
  }
});
