import { getCircuitBlobData } from "../../service/database/rrcs-blob";
import { Router } from "express";
import { getCircuitV2BlobData } from "../../service/database/rrcs-v2-blob";

export const oembedRouter = Router();

export const RRCS_WEBGL_URL = process.env.RRCS_WEBGL_URL;
export const RRCS_WEBGL_CIRCUIT_URL = process.env.RRCS_WEBGL_CIRCUIT_URL;
export const RRCS_OEMBED_PROVIDER = process.env.RRCS_OEMBED_PROVIDER;
export const RRCS_SHARE_CIRCUIT_URL = process.env.RRCS_SHARE_CIRCUIT_URL;

export const RRCS_V2_OEMBED_PROVIDER = process.env.RRCS_V2_OEMBED_PROVIDER;
export const RRCS_V2_SHARE_CIRCUIT_URL = process.env.RRCS_V2_SHARE_CIRCUIT_URL;

oembedRouter.get("/rrcs-blob/:blobId", async (req, res) => {
  try {
    const blob = await getCircuitBlobData(req.params.blobId);
    if (blob) {
      res.json({
        author_name: blob.name || "Unnamed Circuit",
        author_url: RRCS_SHARE_CIRCUIT_URL + req.params.blobId,
        provider_name: "Rec Room Circuit Simulator",
        provider_url: "https://gitlab.com/rrcs/RRCS/",
      });
    } else {
      res.status(404).send("blob not found");
    }
  } catch (err) {
    console.error(err);
    res.status(404).send("blob not found");
  }
});

oembedRouter.get("/rrcs-v2-blob/:blobId", async (req, res) => {
  try {
    const blob = await getCircuitV2BlobData(req.params.blobId);
    if (blob) {
      res.json({
        author_name: blob.name || "Unnamed Circuit",
        author_url: RRCS_V2_SHARE_CIRCUIT_URL + req.params.blobId,
        provider_name: "Rec Room Circuit Simulator V2",
        provider_url: "https://gitlab.com/rrcs/rrcs-v2/",
      });
    } else {
      res.status(404).send("blob not found");
    }
  } catch (err) {
    console.error(err);
    res.status(404).send("blob not found");
  }
});
