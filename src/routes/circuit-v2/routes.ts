import { Router } from "express";
import {
  RRCS_V2_OEMBED_PROVIDER,
  RRCS_V2_SHARE_CIRCUIT_URL,
} from "../oembed/routes";
import { sample } from "lodash";
import { getCircuitV2BlobData } from "../../service/database/rrcs-v2-blob";

export const circuitV2DiscordRouter = Router();

circuitV2DiscordRouter.get(
  ["/:circuitId", "/:circuitId/*"],
  async (req, res, next) => {
    const circuitId = req.params.circuitId;
    const redirectUrl = RRCS_V2_SHARE_CIRCUIT_URL + circuitId;

    if (req.headers["user-agent"] && req.headers["user-agent"].match(/bot/)) {
      try {
        const blob = await getCircuitV2BlobData(circuitId);
        if (blob) {
          res.render("discordmeta", {
            imageUrl: blob.thumbnailUrl,
            color: sample([
              "#D43232",
              "#4FAD3D",
              "#1E72CE",
              "#99FF29",
              "#16DDDB",
              "#E8497F",
            ]),
            description: blob.description,
            oEmbedUrl: RRCS_V2_OEMBED_PROVIDER + circuitId,
            redirectUrl,
          });
        } else {
          res.status(404).send("blob not found");
        }
      } catch (err) {
        console.error(err);
        res.status(404).send("blob not found");
      }
    } else {
      next();
    }
  }
);
