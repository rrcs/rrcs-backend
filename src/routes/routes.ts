import { Router } from "express";
import { oembedRouter, RRCS_WEBGL_URL } from "./oembed/routes";
import { circuitRouter } from "./circuit/routes";
import { rrcsBlobRouter } from "./rrcs-blob/routes";
import { rrcsBlobV2Router } from "./rrcs-v2-blob/routes";
import { frontendProxy } from "../service/frontend-proxy";
import { circuitV2DiscordRouter } from "./circuit-v2/routes";

export const rootRouter = Router();

// API
rootRouter.use("/rrcs-blob/", rrcsBlobRouter);
rootRouter.use("/rrcs-v2-blob/", rrcsBlobV2Router);
rootRouter.use("/oembed/", oembedRouter);

// Discordbot routing
rootRouter.use("/circuit/", circuitRouter);
rootRouter.use("/v2/circuit/", circuitV2DiscordRouter);

// Webapps
rootRouter.use("/v2/", (req, res) => {
  res.redirect(process.env.RRCS_V2_URL);
});
rootRouter.get("/pixel-art-optimizer/", (req, res) =>
  res.redirect("/rr-pixel-art-optimizer/")
);
rootRouter.use(
  "/rr-pixel-art-optimizer/",
  frontendProxy(process.env.PIXEL_ART_OPTIMIZER_URL)
);

// RRCS redirect
rootRouter.get("/", (req, res) => {
  res.redirect(RRCS_WEBGL_URL);
});
