import { getCircuitBlobData } from "../../service/database/rrcs-blob";
import { RRCS_OEMBED_PROVIDER, RRCS_WEBGL_CIRCUIT_URL } from "../oembed/routes";
import { sample } from "lodash";
import { Router } from "express";

export const circuitRouter = Router();

circuitRouter.get("/:blobId", async (req, res) => {
  const redirectUrl = RRCS_WEBGL_CIRCUIT_URL + req.params.blobId;

  if (req.headers["user-agent"] && req.headers["user-agent"].match(/bot/)) {
    try {
      const blob = await getCircuitBlobData(req.params.blobId);
      if (blob) {
        res.render("discordmeta", {
          imageUrl: blob.thumbnailUrl,
          color: sample([
            "#D43232",
            "#4FAD3D",
            "#1E72CE",
            "#99FF29",
            "#16DDDB",
            "#E8497F",
          ]),
          description: blob.description,
          oEmbedUrl: RRCS_OEMBED_PROVIDER + req.params.blobId,
          redirectUrl,
        });
      } else {
        res.status(404).send("blob not found");
      }
    } catch (err) {
      console.error(err);
      res.status(404).send("blob not found");
    }
  } else {
    res.redirect(redirectUrl);
  }
});
