import { Router } from "express";
import {
  createCircuitV2Blob,
  getCircuitV2BlobData,
} from "../../service/database/rrcs-v2-blob";

export const rrcsBlobV2Router = Router();

rrcsBlobV2Router.get("/:blobId", async (req, res) => {
  try {
    const blob = await getCircuitV2BlobData(req.params.blobId);
    if (blob) {
      res.send(blob);
    } else {
      res.status(404).send(`circuit "${req.params.blobId}" not found`);
    }
  } catch (err) {
    console.error(err);
    res.status(404).send(`circuit "${req.params.blobId}" not found`);
  }
});

rrcsBlobV2Router.post("/", async (req, res) => {
  try {
    if (!req.body.data || !req.body.data.length) {
      res.status(400).send("missing data");
    } else if (!req.body.thumbnail || !req.body.thumbnail.length) {
      res.status(400).send("missing thumbnail");
    } else {
      const id = await createCircuitV2Blob(
        req.body.data,
        req.body.thumbnail,
        req.body.name,
        req.body.description,
        req.body.previousVersion
      );

      res.send(id);
    }
  } catch (err) {
    console.error(err);
    res.status(500).send("error");
  }
});
